from typing import Dict, Union

from django.db import IntegrityError

from todo.models import User
from todo.serializer import UserSerializer


def get_user(messeger_id: int) -> Union[User, None]:
    """

    Args:
        messeger_id: messenger id from request

    Returns:
        User object
    """
    try:
        user = User.objects.get(messenger_id=messeger_id)
    except User.DoesNotExist:
        return None
    return user


def add_user(payload: Dict) -> Dict:
    """
    Add user to the Db

    Args:
        payload: User data from payload

    Returns:
        Added user
    """
    try:
        data = dict()
        data["first_name"] = payload["first name"]
        data["messenger_id"] = payload["messenger user id"]
        user = User(**data)
        user.save()
    except IntegrityError:
        raise IntegrityError("User already exist")
    serialized = UserSerializer(user)
    return serialized.data


def delete_user(user: int):
    """

    Args:
        user: fb messenger id

    Returns:
        removed user
    """
    user = User.objects.filter(messenger_id=user).delete()
    serialized = UserSerializer(user)
    return serialized.data
