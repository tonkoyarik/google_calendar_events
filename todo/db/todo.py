from typing import Dict, List, Union

from todo.models import Todo, User
from todo.serializer import TodoSerializer


def add_todo(payload: Dict, user: User) -> Dict:
    """
    Add new task
    Args:
        payload: todo data
        user: user messenger id

    Returns:
        db object
    """
    new_rec = Todo(
        title=payload["title"],
        image_url=payload["image_url"],
        description=payload["description"],
        date_time=payload["date_time"],
        reporter=user,
    )
    new_rec.save()
    serialized = TodoSerializer(new_rec)
    return serialized.data


def remove_todo(task: int) -> Union[Dict, None]:
    """
    Remove task from db
    Args:
        task: task id

    Returns:
        db object
    """
    todo = Todo.objects.filter(id=task).delete()
    if todo[0] == 0:
        return None
    return todo


def get_todo_with_user(user: User) -> List:
    """
    Args:
        user: User object

    Returns:
        All user todos
    """

    todo = Todo.objects.filter(reporter=user)
    serialized = TodoSerializer(todo, many=True)
    return serialized.data


def get_single_todo(todo: int) -> Union[Dict, None]:
    """
    Args:
        todo: id

    Returns:
        Todo object
    """
    try:
        todo = Todo.objects.get(id=todo)
    except Todo.DoesNotExist:
        return None
    serialized = TodoSerializer(todo)
    return serialized.data
