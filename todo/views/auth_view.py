import os

import google_auth_oauthlib.flow
from django.shortcuts import redirect
from django.urls import reverse

from todo.helper import credentials_to_dict, get_redirect_url
from website.settings import GOOGLE_CLIENT_SECRETS_FILE, GOOGLE_SCOPES


def authorize(request):
    # Create flow instance to manage the OAuth 2.0 Authorization Grant Flow steps.
    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        GOOGLE_CLIENT_SECRETS_FILE, scopes=GOOGLE_SCOPES
    )
    flow.redirect_uri = get_redirect_url(request)

    authorization_url, state = flow.authorization_url(
        # Enable offline access so that you can refresh an access token without
        # re-prompting the user for permission. Recommended for web server apps.
        # access_type="offline",
        # Enable incremental authorization. Recommended as a best practice.
        include_granted_scopes="true",
    )

    # Store the state so the callback can verify the auth server response.
    request.session["state"] = state

    return redirect(authorization_url)


def oauth2callback(request):
    # Specify the state when creating the flow in the callback so that it can
    # verified in the authorization server response.
    state = request.session["state"]

    flow = google_auth_oauthlib.flow.Flow.from_client_secrets_file(
        GOOGLE_CLIENT_SECRETS_FILE, scopes=None, state=state
    )
    flow.redirect_uri = get_redirect_url(request)
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"
    # Use the authorization server's response to fetch the OAuth 2.0 tokens.
    authorization_response = request.get_raw_uri()
    flow.fetch_token(authorization_response=authorization_response)

    # Store credentials in the session.
    # ACTION ITEM: In a production app, you likely want to save these
    #              credentials in a persistent database instead.
    credentials = flow.credentials
    request.session["credentials"] = credentials_to_dict(credentials)

    return redirect(reverse("home"))
