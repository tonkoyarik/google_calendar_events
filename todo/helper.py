from itertools import chain, islice
from typing import List, Dict

from django.contrib.sites.shortcuts import get_current_site
from django.urls import reverse

added_todo = {
    "messages": [
        {"text": "You have successfully added a new task, let's check the list now!"}
    ]
}


error_todo = {
    "messages": [
        {
            "text": "I need you to enter Title, Description and DataTime in format (dd/mm/yy 23:50)"
        }
    ]
}

user_not_exist = {
    "messages": [
        {
            "text": "User with your messenger_id does not exist, please clear the message history and start again"
        }
    ]
}
task_removed = {"messages": [{"text": "Selected task has been successfully removed"}]}


def prepare_elements_data(todo_list: List) -> List:
    """

    Args:
        todo_list: List of todos

    Returns:
        List of todos adapted for response

    """
    for todo in todo_list:
        todo[
            "image_url"
        ] = "https://s3-us-west-2.amazonaws.com/slack-files2/avatars/2015-12-26/17403075280_024490441c688e6ab5f8_512.png"
        date_time = todo["date_time"]
        description = todo["description"]
        todo["description"] = f"{description} \n Date: {date_time}"
        task_id = todo.pop("id")
        todo["buttons"] = [
            {
                "url": f"https://ec2-18-219-78-8.us-east-2.compute.amazonaws.com/delete/{task_id}/",
                "type": "json_plugin_url",
                "title": "{}".format("Remove"),
            }
        ]
        del todo["date_time"]
    return todo_list


def convert_one_element(todo: Dict):
    title = todo.get("title")
    description = todo.get("description")
    date_time = todo.get("date_time")
    task_id = todo.pop("id")
    res = {
        "messages": [
            {
                "attachment": {
                    "type": "template",
                    "payload": {
                        "template_type": "button",
                        "text": f"This is your first Event Subject:\n{title}\n:{description}\n Date:{date_time}",
                        "buttons": [
                            {
                                "url": f"https://ec2-18-219-78-8.us-east-2.compute.amazonaws.com/delete/{task_id}/",
                                "type": "json_plugin_url",
                                "title": "{}".format("Remove"),
                            }
                        ],
                    },
                }
            }
        ]
    }
    go_back = {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "button",
                "text": "Add another todo",
                "buttons": [
                    {
                        "type": "show_block",
                        "block_names": ["Add task"],
                        "title": "Назад",
                    }
                ],
            },
        }
    }
    res["messages"].append(go_back)
    return res


def convert_elements_to_fb_list(elements):
    go_back = {
        "attachment": {
            "type": "template",
            "payload": {
                "template_type": "button",
                "text": "Go back",
                "buttons": [
                    {
                        "type": "show_block",
                        "block_names": ["Todo list"],
                        "title": "Back",
                    }
                ],
            },
        }
    }
    res = {"messages": []}
    for chunk in chunks(elements, 3):
        list_4 = {
            "attachment": {
                "type": "template",
                "payload": {
                    "template_type": "list",
                    "top_element_style": "large",
                    "elements": list(chunk),
                },
            }
        }
        res["messages"].append(list_4)
    res["messages"].append(go_back)
    return res


def chunks(iterable: iter, n: int) -> iter:
    """
    Generator that yields successive n-sized chunks from an iterable.
    Each yilded chunk is a generator of size=chunk_size ().
    That means you have to empty (iterate over) current chunk in order to get the right result
    Last chunk will be sized as is and will not be padded with any value.
    ABCDEFGH -> ABC, DEF, GH
    >>> for chunk in chunks(range(25), n=7):
    ...     print(list(chunk))
    [0, 1, 2, 3, 4, 5, 6]
    [7, 8, 9, 10, 11, 12, 13]
    [14, 15, 16, 17, 18, 19, 20]
    [21, 22, 23, 24]
    """
    iterator = iter(iterable)
    for first in iterator:
        yield chain([first], islice(iterator, n - 1))


def todo_list_response(todo_list: List) -> Dict:
    """

    Args:
        todo_list: List of todos

    Returns:
        Json (dict) response in required format
    """
    if len(todo_list) == 1:
        todo = todo_list[0]

        response = convert_one_element(todo)
        return response
    else:
        todo_elements = prepare_elements_data(todo_list)

        return convert_elements_to_fb_list(todo_elements)


def credentials_to_dict(credentials):
    creds = {
        "token": credentials.token,
        "refresh_token": credentials.refresh_token,
        "token_uri": credentials.token_uri,
        "client_id": credentials.client_id,
        "client_secret": credentials.client_secret,
        "scopes": credentials.scopes,
    }
    return creds


def get_redirect_url(request):
    domain = get_current_site(request).domain
    url = reverse("oauth2callback")
    return f"http://{domain}{url}"
