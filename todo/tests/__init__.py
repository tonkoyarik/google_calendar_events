from todo.models import User, Todo


def setup():
    user = User(messenger_id="874323873872623", first_name="Yarik")
    user.save()
    todo = Todo(
        title="test title",
        image_url="http://www.halo.com",
        description="subtitle1",
        date_time="2017-12-17 23:50",
        reporter=user,
    )
    todo.save()
    user_3 = User(messenger_id="28736882827648", first_name="John")
    user_3.save()
    todo_2 = Todo(
        title="test title_2",
        image_url="http://www.halo.com",
        description="subtitle1",
        date_time="2017-12-17 23:50",
        reporter=user_3,
    )
    todo_2.save()
    user_4 = User(messenger_id="984976237834", first_name="Nick")
    user_4.save()
    todo_3 = Todo(
        title="test title_3",
        image_url="http://www.test2.com",
        description="subtitle2",
        date_time="2013-11-17 23:50",
        reporter=user_3,
    )
    todo_3.save()
