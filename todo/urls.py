from django.urls import path

from todo.views import todo_view, auth_view
from django.contrib import admin


urlpatterns = [
    path("", todo_view.UserView.as_view(), name="home"),
    path("admin/", admin.site.urls),
    path("todo/", todo_view.TodoView.as_view()),
    path("welcome/", todo_view.UserView.as_view()),
    path("delete/<int:task_id>/", todo_view.Delete.as_view(), name="remove task"),
    path("oauth2callback/", auth_view.oauth2callback, name="oauth2callback"),
    path("authorize/", auth_view.authorize, name="authorize"),
]
