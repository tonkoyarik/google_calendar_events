
[![pipeline status](https://gitlab.com/tonkoyarik/Django_bot_todo/badges/master/pipeline.svg)](https://gitlab.com/tonkoyarik/Django_bot_todo/commits/master)

1) install requirements:

 ```pip install -r requirements.txt```

2) create a postgres database: 

```sudo -u postgres createdb todo_bot```

3) migrate db:

```python manage.py makemigrations```
```python manage.py migrate```

Now you can run a server:

```python manage.py runserver```